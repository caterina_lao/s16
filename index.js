                 /*LOOPS*/
/*Instruction: Display Juan Dela Cruz on our console 10x*/

console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");

/*INstruction: Display each element available on our array*/

let students = ["TJ", "Mia", "Tin", "Chris"];

console.log(students[0]);
console.log(students[1]);
console.log(students[2]);
console.log(students[3]);

/*While Loop*/

let count = 5; /*number of the iteration, number of times of how many we repeat our code*/
// while (/*Condition*/) { condition - evaluates a given code if its true or false - if the condition is true, the loop will start and continue our iteration or the repetition of our block of code, but if the condition is false, the loop or the repetition will stop
// 	// block of code - this will be repeated by the loop
// 	// counter for our interation - this is the reason of our continuous loop iteration
// }

// example: (Instruction: Repeat a name"Sylvan" 5x)

// 5 !==0 - true
while(count !== 0){ //condition - if count value if not equal to zero
	console.log("Sylvan");
	count--; //will be decremented by 1
}

/*Instruction: Print numbers 1 to 5 using while*/
let number = 1;
	// 1 <=5 - true
	//2 <=5 - true
while(number <= 5){ //if the number is greater than or equal to 5
	console.log(number);
	number++; //1+1 = 2
}

/*
	1
	2
	3
	4
	5
*/

/*Instruction: with a given array, kindly print each element using while loop*/

let fruits = ['Banana','Mango'];
//fruits[0]
//fruits[1]

let indexNumber = 0; //We will use this variable as our reference to the index position/number of our given array

while(indexNumber <=1) { // the condition is based on the last index of elements that we have on an array
	console.log(fruits[indexNumber]) // kukuhanin natin yung elements sa loob ng array base sa indexNumber value
	indexNumber++;
}

let mobilePhones = ['Samsung Galaxy S21', 'Iphone 13 Pro', 'Xioami 11T', 'Realme C', 'Huawei Nova 8', 'Pixel 5', 'Asus Rog 6', 'Nokia', 'Cherry Mobile'];

console.log(mobilePhones.length);
console.log(mobilePhones.length -1); //will give us the last index position of an element in an array
console.log(mobilePhones[mobilePhones.length - 1]); //get the last element of an array

let indexNumberForMobile = 0;

while(indexNumberForMobile <=mobilePhones.length-1) { //standard formula/condition
	console.log(mobilePhones[indexNumberForMobile]);
	indexNumberForMobile++;
}

/*Do-while Loop  - do the statement once before going to the condition*/

let countA = 1;

do { //it executes the statement first
	console.log("Juan");
	countA++; //1+1=2 ; 2+1=3 ; 3+ 1=4; 4+1=5; 5+1=6; 6+1=7
} while (countA <=6); //2 <= 6 ? true ; 3 >=6? true; 4 <=6 ? true; 5 <=6? true ; 6 <=6? true ; 7 <= 6 ?false (stop na ang loop)

/*
	Juan
	Juan
	Juan
	Juan
	Juan
	Juan
*/
console.log("==========Do-While versus While==========");

let countB = 6;

do {
	console.log(`Do-While count ${countB}`);
	countB--;
}while (countB == 7);

/*Versus*/

while(countB ==7) {
	console.log(`While count ${countB}`);
	countB--;
}

/*
	Mini activity
	Instruction: with a given array, kindly dsplay each elements on the console using do-while loop
*/

let indexNumberA = 0;
let computerBrands = ['Apple Macbook Pro', 'HP Notebook', 'Asus', 'Lenovo', 'Acer', 'Dell', 'Huawei'];

do {
	console.log(computerBrands[indexNumberA]);
	indexNumberA++;
} while(indexNumberA <=computerBrands.length-1);

/*For Loop - well organized*/

//variable (let count=5;) - the scope of the declared variable is within the FOR LOOP 
//condition (count >=0;)
// iteration - decrement (count--)

// 
for (let count = 5; count >= 0; count--) {
	console.log(count);
}

/*Instruction: Given an array, kindly print each element using for loop*/

let colors = ['Red', 'Green','Blue', 'Yellow','Purple', 'White', 'Black'];

for (let i =0; i <= colors.length-1; i++){
	console.log (colors[i]);
} 

// Continue & Break
// Breaks - stops the execution of our code
// Continue - skip a block code and continue to the next iteration

/*
ages
	18, 19, 20, 21, 24, 25
	
	age == 21 (debutante age of boys), we will skip then got to the next iteration

	18, 19, 20, 24, 25

*/

let ages = [18, 19, 20, 21, 24, 25];
/*Skip the debutante age of boys and girls using continue keyword*/

for ( let i = 0; i <= ages.length-1; i++) {
	if(ages[i] == 21 || ages[i] == 18) {
		continue; //means skipped ang 18 and 21
	}
	console.log(ages[i]);
}

/*Break*/

/*
	let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];

	Once we find Jayson on our array, we will stop the loop.

	Read/Print:
	Den
	Jayson (loop stopped)
*/
let studentNames =['Den', 'Jayson', 'Marvin', 'Rommel'];

for (let i = 0; i <= studentNames.length-1; i++) {
	if (studentNames[i] == "Jayson") {
		console.log(studentNames[i])
		break;
	}
}

//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

/*
	Sample output:
  
  20
  23
  33
  27
  70
  55
  63
  85
*/
// Solution:
for ( let i = 0; i <= adultAge.length-1; i++) {
	if(adultAge[i] <= 19) {
		continue; //means skipped ang 18 and 21
	}
	console.log(adultAge[i]);
}

/*
Instructions:

2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
	the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/
let students1 = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(students1){
	for (let i = 0; i <= students1.length-1; i++) {
		if (students1[i] == "Jazz") {
			console.log(students1[i])
			break;	
		}
	}
}

searchStudent('Jazz'); //invoked function with a given argument 'Jazz'


/*
	Sample output:
  
  Jazz
*/